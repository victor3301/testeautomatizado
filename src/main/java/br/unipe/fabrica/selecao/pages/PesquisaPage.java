package br.unipe.fabrica.selecao.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import br.unipe.fabrica.selecao.drvs.DriverManipulation;

public class PesquisaPage {
	private DriverManipulation _drv;
	
	// ******** Construtor ********
	public PesquisaPage(WebDriver drv) {
		_drv = new DriverManipulation(drv);
	}
	
	
	//Elementos da p�gina
	
	@FindBy(xpath = "/html/body/div/div[3]/form/div[2]/div/div[1]/div/div[1]/input")
	private WebElement CAMPO_PESQUISA;
	
	@FindBy(xpath = "/html/body/div/div[3]/form/div[2]/div/div[3]/center/input[1]")
	private WebElement BOTAO_PESQUISA;
	
	@FindBy(xpath = "/html/body/div[6]/div[3]/div[10]/div[1]/div[3]/div/div/div/div[1]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div/div/div[2]/div[1]/span")
	private WebElement RESULTADO;
	
	
	//M�todos
	
	//M�todo para o campo de pesquisar
	public void pesquisar(String value) {
		_drv.preencherCampo(CAMPO_PESQUISA, value);
	}
	
	//metodo para o bot�o de clicar
	public void clicar_botao() {
		_drv.clica(BOTAO_PESQUISA);
	}
	
	//o resultado, ou mensagem do sistema
	public boolean resultado() {
		return _drv.checa(RESULTADO, "Automa��o de teste");
	}
	


	

	


}
