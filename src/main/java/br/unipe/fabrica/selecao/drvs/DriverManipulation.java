package br.unipe.fabrica.selecao.drvs;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverManipulation {
	private WebDriver drv;

	public DriverManipulation(WebDriver drv) {
		this.drv = drv;
	}

	public void navegarPara(String url) {
		drv.navigate().to(url);
	}

	public void preencherCampo(WebElement element, String text) {
		element.sendKeys(text);
	}

	public void clica(WebElement element) {
		element.click();
	}

	public void duploclique(WebElement element) {
		element.click();
		element.click();
	}

	public void selecionaValorVisivel(WebElement element, String text) {
		Select combo = new Select(element);
		combo.selectByVisibleText(text);
	}

	public void espera(long tempo) throws InterruptedException {
		wait(tempo);
	}

	public boolean checa(By element, String text) {
		try {
			WebElement el = drv.findElement(element);
			return el.getText().startsWith(text);
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public boolean checa(WebElement element, String text) {
		try {
			return element.getText().startsWith(text);
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public boolean explicitWait_present(By element, long timeOutInSeconds) {
		WebDriverWait wait = new WebDriverWait(drv, timeOutInSeconds);
		WebElement el = wait.until(ExpectedConditions.presenceOfElementLocated(element));
		return el instanceof WebElement;
	}

	public boolean explicitWait_clickable(By element, long timeOutInSeconds) {
		WebDriverWait wait = new WebDriverWait(drv, timeOutInSeconds);
		WebElement el = wait.until(ExpectedConditions.elementToBeClickable(element));
		return el instanceof WebElement;
	}

}
