package br.unipe.fabrica.testes.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.support.PageFactory;


import br.unipe.fabrica.selecao.pages.PesquisaPage;
import br.unipe.fabrica.testes.base.BaseTest;

public class TestePesquisa extends BaseTest {

	protected PesquisaPage page;
	static final String SITE = "https://www.google.com/";
	String PESQUISA = "testes automatizados";

	
	//Aqui � a classe de teste
	
	public TestePesquisa() {
		super(SITE);
		if (this.getDrv() == null) {
			System.exit(-1);
		} else {
			page = PageFactory.initElements(this.getDrv(), PesquisaPage.class);
		}
	}
	
	
	@Test
	public void testPesquisa() {
		page.pesquisar(PESQUISA);
		page.clicar_botao();
		assertTrue(page.resultado());

	}
	
}
