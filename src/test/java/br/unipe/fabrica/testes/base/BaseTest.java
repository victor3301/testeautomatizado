package br.unipe.fabrica.testes.base;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import br.unipe.fabrica.selecao.utils.TestLogger;
import br.unipe.fabrica.selecao.utils.Utils;

public abstract class BaseTest {

	private WebDriver driver;
	private String target;

	// ******** JUnit Annotations
	@Rule
	public TestWatcher watchdog = new TestLogger();

	@BeforeClass
	public static void init() {
		Utils.log().info("In�cio dos testes");
	}

	@AfterClass
	public static void quit() {
		Utils.log().info("Fim dos testes");
	}

	@Before
	public void hi() {
		driver.get(Target());
	}

	@After
	public void bye() {
		driver.close();
	}

	// ******** constructors
	private BaseTest() {
		String driver = "";
		if(Utils.osIsLinux()) driver = "/geckodriver";
		if(Utils.osIsWindows()) driver = "\\geckodriver.exe";
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + driver);
		if(Utils.osIsLinux()) driver = "/chromedriver";
		if(Utils.osIsWindows()) driver = "\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + driver);
	}

	public BaseTest(WebDriver drv, String str, long tempo) {
		this();
		driver = drv;
		driver.manage().timeouts().implicitlyWait(tempo, TimeUnit.SECONDS);
		Target(str);
	}

	public BaseTest(WebDriver drv, String str) {
		this(drv, str, 0);
	}

	public BaseTest(String str) {
		this(new FirefoxDriver(), str);
	}

	// ******** getters & setters
	public WebDriver getDrv() {
		return driver;
	}

	public String Target() {
		return target;
	}

	public void Target(String url) {
		target = url;
	}

}
