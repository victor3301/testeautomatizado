# Código-base para construção dos testes

## Classes renomeadas para melhor descrição dse suas responsabilidades

- *Base*: antiga "PaiDeTodas". As classes "*Test" devem estender esta classe; ela determina o comportamento do teste. Seus construtores foram escritos prevendo a possibilidade de fácil configuração dos parâmetros considerados importantes (configura local do driver, driver a instanciar, endereço da aplicação-alvo dos testes, tempo para implicitWait); descreve uma regra, que serve para implementar o _Logger_. Foi refatorada, promovendo manutenibilidade, pois foi desacoplada da classe _DriverManipulation_, de quem mantinha uma instância.

- *DriverManipulation*: antiga "DSL". O termo "DSL" é usado no sentido explicado [aqui](https://pt.wikipedia.org/wiki/Linguagem_de_dom%C3%ADnio_espec%C3%ADfico ), por isso a mudança de nome da classe. Ela implementa as ações gerais que o usuário pode executar numa página web (clicar num elemento, preencher um campo de texto, selecionar um valor numa combobox, etc.).

- *"*Page"*: antiga "Page". Esta classe é a abstração do usuário, contendo as ações direcionadas para a aplicação-alvo dos testes (preencher campo Email, clicar no link para excluir cadastro). Responsável por instanciar a classe _DriverManipulation_, pois apenas ela deve usá-la.

- *"*Test"*: sub-classe de _Base_, ela é responsável por instanciar a classe _PageObject_, que fornece os métodos que executam as sequências de ações previstas pelos casos de teste. Pode manter uma lista de parâmetros para uso durante a execução dos métodos anotados com `@Test`.

- *TestLogger*: estende de _TestWatcher_, com sobrescrita dos métodos `failed()` e `succeeded()`, é responsável por registrar falhas e sucessos dos testes.

- *Utils*: classe de métodos estáticos, que facilitam e reduzem código a ser escrito, como manipulação de datas, por exemplo. Mantém a instância do _Logger_, usado para registrar situações durante os testes.

_Próximos passos_: Possibilitar configuração dos testes por **TestSuite**
